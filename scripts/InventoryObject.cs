﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryObject : MonoBehaviour {
    public bool can_add = true; //true : l'objet peut être rangé
    public Sprite inventory_sprite;
    public string item_name = "";
    public string description = "";
    public int weight = 0;
    public bool usable;
	public void Interaction()
    {
        //inventory_sprite = this.GetComponent<SpriteRenderer>().sprite;
        this.gameObject.SetActive(false);
    }
}
