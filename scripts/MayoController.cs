﻿using UnityEngine;

/*
 * TODO : 
 * -infinite jump OK
 * -takeobject
 */
public class MayoController : MonoBehaviour
{
    public float Speed = 4;
    public float jumpVelocity = 10;
    public LayerMask playerMask;

    [HideInInspector]
    public Transform myTrans { get; set; }
    [HideInInspector]
    public Rigidbody2D myBody { get; set; }
    [HideInInspector]
    public CapsuleCollider2D myCollider { get; set; }

    /*
     * Touche vérification
     */
    [HideInInspector]
    public bool canJump = false;


    public bool isJumping { get; set; }
    public bool isGround { get; set; }
    public bool take { get; set; }
    public bool launch { get; set; }
    public bool down { get; set; }
    public bool take_in_bag { get; set; }
    public bool isGrabbing { get; set; }
    public bool block_mov { get; set; }
    /*
     * Walls var & grab & no stuck in wall
     * 
     */


    private Transform tagGroundL, tagGroundR, tagGrabRight, tagGrabLeft;
    [HideInInspector]
    public float direction { get; set; } //Négatif = Mayo dirigé vers la gauche, sinon vers la droite
    public bool directed = false;
    public float oldDirection;
    public bool directionHasChanged;


    /*
     * 
     * Descending Slopes
     * */
    public float maxDescendAngle = 75;
    private float angle;
    const float skinWidth = .015f;

    /*
     * Anti Sliding
     * */
    public bool moving = false;
    /*
     * Objets de Mayo
     */



    private Transform objectTransS; //Le petit transform que Mayo a choppé


    /*
     * Modules
     */
    //Checkpoints
    [HideInInspector]
    public Vector3 respawn_point;
    //Wall gestion
    private WallCollision WallCollision;
    [HideInInspector]
    public GameObject wall { get; set; }
    [HideInInspector]
    public SpriteRenderer wall_renderer { get; set; }
    [HideInInspector]
    public Transform wall_transform { get; set; }
    private Transform wall_detector_transform;
    public bool direction_wall;
    public bool grabable_wall;
    //Prendres des objets
    private ObjectCarrying ObjectCarrying;
    [HideInInspector]
    public GameObject object_locked { get; set; }
    [HideInInspector]
    public Transform object_locked_trans;
    [HideInInspector]
    public Rigidbody2D object_locked_rb;

    [HideInInspector]
    private Transform ray_obj_start;
    public float ray_dist_action_y;
    public float ray_dist_action_x;
    //Mode Buisson
    public bool inside_buisson { get; set; }
    private Transform trans_buisson;
    //Inventaire
    public Inventory bag;
    //Lock
    private Transform lock_trans;
    [HideInInspector]
    public bool locking = false;

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */





    //------------------------------------------------------------------------------------------------------------------------------------
    void initObject()
    {
        WallCollision = new WallCollision(this);
        wall = null;
        ObjectCarrying = new ObjectCarrying(this);
        object_locked = null;

        isGround = false;
        isJumping = false;
        take = false;
        launch = false;
        down = false;
        isGrabbing = false;
        inside_buisson = false;
        direction_wall = false;
        block_mov = false;

        myBody = this.GetComponent<Rigidbody2D>();
        myTrans = this.transform;
        myCollider = this.GetComponent<CapsuleCollider2D>();
        tagGroundL = GameObject.Find(this.name + "/tagGroundL").transform;
        tagGroundR = GameObject.Find(this.name + "/tagGroundR").transform;
        playerMask = GameObject.Find(this.name).layer;
        ray_obj_start = GameObject.Find(this.name + "/ObjectRay").transform;
        wall_detector_transform = GameObject.Find(this.name + "/WallDetector").transform;
        bag = this.GetComponent<Inventory>();
        lock_trans = GameObject.Find("Spécials/Lock").GetComponent<Transform>();
        respawn_point = new Vector3(-3.93f, 1.9f, 0);
    }
    void generalSetup()
    {
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Mob"), LayerMask.NameToLayer("Object"), true);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Mob"), LayerMask.NameToLayer("Mob"), true);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("PNJ"), LayerMask.NameToLayer("Player"), true);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Mob"), LayerMask.NameToLayer("Buisson"), true);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Buisson"), LayerMask.NameToLayer("Object"), true);
    }
    void Start()
    {
        initObject();
        generalSetup();    
    }
    //------------------------------------------------------------------------------------------------------------------------------------
    private void Update()
    {
        
        RaycastHit2D[] hits = new RaycastHit2D[3];
        int h = Physics2D.RaycastNonAlloc(myTrans.position, -Vector2.up, hits);
        if (h > 1)
        { //if we hit something do stuff

            angle = Mathf.Abs(Mathf.Atan2(hits[2].normal.x, hits[2].normal.y) * Mathf.Rad2Deg); //get angle

            if (angle > 1)
            {
                //Debug.Log("Angle :" + angle);

            }
        }
        if(!block_mov)
            checkKeyPressedUpdate();
        objectRaycasting();
    }

    public void objectRaycasting()
    {
        //Vector2 v2 = new Vector2(0, -ray_distance_action);
        RaycastHit2D obj_ray_info = Physics2D.Raycast(ray_obj_start.transform.position, Vector2.down, ray_dist_action_y,
            1 << LayerMask.NameToLayer("Object")| 1 << LayerMask.NameToLayer("PNJ"));
        Debug.DrawRay(ray_obj_start.transform.position, Vector2.down, Color.red);
        if (obj_ray_info.transform != null)
        {
            if (obj_ray_info.transform.gameObject.tag == "PortableObjectS")
            {
                Lock(obj_ray_info.transform.gameObject);
                setObjectLocked(obj_ray_info.transform.gameObject, obj_ray_info.transform, obj_ray_info.rigidbody);
                
            }
            else if (obj_ray_info.transform.gameObject.tag == "InventoryObject")
            {
                
                if(take_in_bag)
                {
                    bool itemAdded = bag.addItem(obj_ray_info.transform.gameObject);
                    if (itemAdded)
                        Debug.Log("Item ajouté : " + obj_ray_info.transform.gameObject.name);
                    else
                        Debug.Log("Impossible de prendre l'objet");
                }
                Lock(obj_ray_info.transform.gameObject);

            }
            else if(obj_ray_info.transform.gameObject.tag == "PNJ" && !block_mov)
            {
                if (take_in_bag)
                    startDialogue(obj_ray_info.transform.gameObject);
            }
            /*else if(obj_ray_info.transform.gameObject.layer == LayerMask.NameToLayer("Buisson") && down)
            {
                
            }*/
            else
            {
                setObjectLocked(null, null, null);
                if(locking)
                    Delock();
            }
        }
        else
        {
            setObjectLocked(null, null, null);
            if (locking)
                Delock();
        }
        //take_in_bag est mis à false ici parce qu'il dépend du raycasting
        take_in_bag = false;
    }
    //------------------------------------------------------------------------------------------------------------------------------------
    public void checkKeyPressedUpdate() //Vérifie les boutons pressés pour ne pas en louper
    {
        // Read the jump input in Update so button presses aren't missed.
        if (!canJump)
        {
            canJump = Input.GetButtonDown("Jump");
        }

        if (Input.GetKey("right") || Input.GetKey("left"))
            moving = true;
        else
            moving = false;

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            take = true;
            //take_in_bag = true;
        }
            
        if (Input.GetKeyUp(KeyCode.A))
            launch = true;
        if (Input.GetKey("down"))
        {
            down = true;
        }
        if (Input.GetKeyUp(KeyCode.E))
        {
            take_in_bag = true;
        }
    }
    private void KeyPressedToFalse()
    {
        take = false;
        launch = false;
        down = false;
        canJump = false;
        
    }
    //------------------------------------------------------------------------------------------------------------------------------------
    void FixedUpdate()
    {

        if (Physics2D.Linecast(tagGroundR.position, tagGroundL.position))
        {
            isGround = true;
            isJumping = false;
        }
        else
        {
            isGround = false;
        }
        direction = Input.GetAxis("Horizontal");
        
        Move(direction);

        oldDirection = direction;
        directionHasChanged = false;
        detectorAndCoDirection();
        //Modules update


        if (inside_buisson)
        {
            FixedUpdateBuisson();
        }
        else
        {
            FixedUpdateNormal();
        }

    }
    //------------------------------------------------------------------------------------------------------------------------------------
    public void FixedUpdateNormal()
    {
        Speed = 4f;

        if (canJump && isGround)
        {
            if (myBody.IsSleeping())
                myBody.WakeUp();
            Jump();
            canJump = false;
        }

        if (wall != null)
        {
            WallCollision.update(canJump);
        }
        else
        {
            antiSlide();
        }
        ObjectCarrying.update(take, directed, launch);

        if (down && trans_buisson != null)
        {
            //Changer de perspective avec le buisson (on a les données)
            //cad, changer le sprite (avec l'animation) de Mayo, changer la vitesse et le collider
            toBuisson();
        }
        KeyPressedToFalse();
    }
    //------------------------------------------------------------------------------------------------------------------------------------
    public void FixedUpdateBuisson()
    {
        antiSlide();
        if (canJump && isGround)
        {
            if (myBody.IsSleeping())
                myBody.WakeUp();

            //Retour à Mayo
            toMayo();
            Jump();
            canJump = false;
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------
    public void Move(float hInput)
    {
        if (!myBody.IsAwake())
            myBody.WakeUp();
        Vector2 moveVel = myBody.velocity;
        moveVel.x = hInput * Speed;
        if (hInput > 0)
            directed = true;
        else if (hInput < 0)
            directed = false;
        if ((myBody.velocity.y < 0) || (myBody.velocity.y > 0))
        {
            myBody.velocity = moveVel;
        }
        else
        {
            myBody.velocity = moveVel;
        }
        isGrabbing = false;
    }

    //------------------------------------------------------------------------------------------------------------------------------------
    public void Jump()
    {

        myBody.velocity = new Vector2(0f, 0f);
        myBody.AddForce(new Vector2(0f, jumpVelocity));
        isJumping = true;
    }

    //------------------------------------------------------------------------------------------------------------------------------------
    public void setPlayerCollider(bool b)
    {
        myCollider.enabled = b;
    }
    //------------------------------------------------------------------------------------------------------------------------------------
    public void DescendSlope(Vector2 velocity)
    {
        if (isGround)
        {
            velocity.y -= (angle / 10);
        }
        myBody.velocity = velocity;
    }
    public void antiSlide()
    {
        WallCollision.has_jumped = false;
        if (!moving && isGround)
        {
            myBody.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
        }
        else
        {
            myBody.constraints = RigidbodyConstraints2D.FreezeRotation;
        }

    }
    public void antiBump()
    {

    }
    //------------------------------------------------------------------------------------------------------------------------------------

    public void detectorAndCoDirection()
    {
        Vector2 newDirectionWallDetector = wall_detector_transform.localPosition;
        Vector2 new_ray_start = ray_obj_start.localPosition;
        if ((newDirectionWallDetector.x < -0.27f) | (newDirectionWallDetector.x > -0.27f))
        {
            if (directed)
            {
                newDirectionWallDetector.x = -0.2059f;
                new_ray_start.x = ray_dist_action_x;
            }
            else
            {
                newDirectionWallDetector.x = -0.28f;
                new_ray_start.x = -ray_dist_action_x;
            }

            ray_obj_start.localPosition = new_ray_start;
            wall_detector_transform.localPosition = newDirectionWallDetector;
        }

    }



    //------------------------------------------------------------------------------------------------------------------------------------
    //Setting up Modules

    public void setWall(GameObject wall, SpriteRenderer wall_renderer, Transform wall_transform, bool direction_wall, bool grabable_wall)
    {
        this.wall = wall;
        this.wall_renderer = wall_renderer;
        this.wall_transform = wall_transform;
        this.direction_wall = direction_wall;
        this.grabable_wall = grabable_wall;
    }
    public void setObjectLocked(GameObject object_locked, Transform object_locked_trans, Rigidbody2D object_locked_rb)
    {
        if (object_locked_rb != null)
        {
            this.object_locked = object_locked;
            this.object_locked_trans = object_locked_trans;
            this.object_locked_rb = object_locked_rb;
        }
        else
        {
            this.object_locked = null;
        }
    }
    public void setBuisson(Transform tb, Rigidbody2D rb, bool nullornot)
    {
        if (!inside_buisson)
        {
            if (nullornot)
            {
                trans_buisson = tb;
            }
            else
            {
                trans_buisson = null;
            }
        }
        else
        {
            Debug.Log("impossible déjà dans un buisson");
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------
    private void toBuisson()//changer le sprite(avec l'animation) de Mayo, changer la vitesse et le collider
    {
        myTrans.gameObject.layer = LayerMask.NameToLayer("Buisson");
        inside_buisson = true;
        Speed = 2.5f;
        Vector3 vscale = myTrans.localScale;
        Vector2 cscale = myCollider.size;
        vscale.y = 0.6f;
        vscale.x = 0.6f;
        vscale.z = 0.03f;
        cscale.y = 0.9921848f;
        cscale.x = 0.8920884f;
        myTrans.localScale = vscale;
        myCollider.size = cscale;
    }

    private void toMayo()
    {
        myTrans.gameObject.layer = LayerMask.NameToLayer("Player");
        inside_buisson = false;
        Speed = 4f;
        Vector3 vscale = myTrans.localScale;
        Vector2 cscale = myCollider.size;
        vscale.y = 0.7f;
        vscale.x = 0.7f;
        vscale.z = 0f;
        cscale.y = 0.9921848f;
        cscale.x = 0.3713819f;
        myTrans.localScale = vscale;
        myCollider.size = cscale;

        setBuisson(null, null, false);
    }
    //------------------------------------------------------------------------------------------------------------------------------------
    public void setCheckpoint(Vector3 checkpoint)
    {
        respawn_point = checkpoint;
    }

    public void respawn()
    {
        myTrans.position = respawn_point;
    }
    //------------------------------------------------------------------------------------------------------------------------------------
    public void Lock(GameObject obj)
    {
        Vector3 v = obj.transform.position;
        //v.z = -3;
        if (!lock_trans.gameObject.activeInHierarchy)
            lock_trans.gameObject.SetActive(true);
        lock_trans.position = v;
        if(obj.layer == LayerMask.NameToLayer("Object"))
            locking = true;
    }
    public void Delock()
    {
       /* Vector3 v = lock_trans.position;
        //v.z = 100;
        lock_trans.position = v;*/
        if (lock_trans.gameObject.activeInHierarchy)
            lock_trans.gameObject.SetActive(false);
        locking = false;
    }
    //------------------------------------------------------------------------------------------------------------------------------------
    private void startDialogue(GameObject pnj)
    {
        block_mov = true;
        pnj.GetComponent<Character>().startDialogue();
    }
    //------------------------------------------------------------------------------------------------------------------------------------
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "FallDetector")
        {
            transform.position = respawn_point;
        }
        else if (other.tag == "Checkpoint")
        {
            Debug.Log("chekpoint");
            respawn_point = other.transform.position;

        }
    }
    void OnTriggerStay2D(Collider2D other)
    {
        if(other.transform.gameObject.layer == LayerMask.NameToLayer("Buisson"))
        {
            if (!locking)
            {
                Lock(other.transform.gameObject);
            }
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.gameObject.layer == LayerMask.NameToLayer("Buisson"))
        {
            Delock();
        }
    }


        /*void OnTriggerExit2D(Collider2D other)
        {
            take_in_bag = false;
        }*/
}
