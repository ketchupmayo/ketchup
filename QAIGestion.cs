﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class QAIGestion : MonoBehaviour {
    private Image item_selected, item_right, item_left;
    private Inventory inventory;
    private InventoryGestion inv_gestion;
    private InventoryObject[] slot_info_us = new InventoryObject[12];
    private Image[] places = new Image[3]; //0 = left, 1 = center, 2 = right
    private int[] free_spaces = new int[3];
    // Use this for initialization
    void Start () {
        inv_gestion = GameObject.Find("InventoryCanvas").GetComponent<InventoryGestion>();
        item_selected = GameObject.Find("ItemSelectedSprite").GetComponent<Image>();
        item_right = GameObject.Find("ItemRight").GetComponent<Image>();
        item_left = GameObject.Find("ItemLeft").GetComponent<Image>();
        inventory = GameObject.Find("Mayo").GetComponent<Inventory>();
        places[0] = GameObject.Find("ItemLeft").GetComponent<Image>();
        places[1] = GameObject.Find("ItemSelectedSprite").GetComponent<Image>();
        places[2] = GameObject.Find("ItemRight").GetComponent<Image>();
        for (int j = 0; j < 3; j++)
            free_spaces[j] = -1;
        CompleteInfo();
        Display();
    }
	
	// Update is called once per frame
	void Update () {
        //FullUpdate();
    }

    public void FullUpdate()
    {
        CompleteInfo();
        Display();
    }
    bool Display()
    {

        for (int i = 0; i < 12; i++)
        {
            if (slot_info_us[i] != null)
            {
                for (int j = 0; j < 3; j++)
                {
                    Debug.Log("Displaying");
                    if (free_spaces[j] == -1 && !IsIn(i))
                    {
                        places[j].sprite = slot_info_us[i].inventory_sprite;
                        free_spaces[j] = i;
                    }
                    else if (!Space())
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    bool IsIn(int i)
    {
        for (int j = 0; j < 3; j++)
        {
            if (free_spaces[j] == i)
                return true;
        }
        return false;
    }
    bool Space()
    {
        for (int j = 0; j < 3; j++)
        {
            if (free_spaces[j] == -1)
                return true;
        }
        return false;
    }
    //------------------------------------------------------------------------------------------------------------------------------------
    public void CompleteInfo()
    {
        if (inventory.items[0] != null && inventory.items[0].GetComponent<InventoryObject>().usable)
            slot_info_us[0] = inventory.items[0].GetComponent<InventoryObject>();//GameObject.Find("Slot").GetComponent<InventoryObject>();
        for (int i = 1; i < 12; i++)
        {
            if (inventory.items[i] != null && inventory.items[i].GetComponent<InventoryObject>().usable)
                slot_info_us[i] = inventory.items[i].GetComponent<InventoryObject>();
        }
    }
}
